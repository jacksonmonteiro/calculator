const textView = document.querySelector('#textView'); //Quadro de exibição

let operador; //Operação matemática
let num1;
let num2;
let resultado; //Resultado a ser exibido

//Define valor inicial a ser mostrado
window.onload = init();

function init(){
	textView.value = 0;
};

//Pega os números clicados e os insere no quadro de exibição
function inserir(numero){
	if (textView.value == 0) {
		textView.value = '';
		num2 = textView.value + numero;
	} else {
		num2 = textView.value + numero;
	}

	textView.value = num2;
};

//realiza as operações
function operacao(ops){
	num1 = num2;
	
	num2 = '';
	textView.value = num2; 
	
	operador = ops;
};

//exibe o resultado
function igual(){
	num1 = Number(num1);
	num2 = Number(num2);

	switch (operador) {
		case '+':
			resultado = num1 + num2;
			break;
		case '-':
			resultado = num1 - num2;
			break;
		case '*':
			resultado = num1 * num2;
			break;
		case '/':
			resultado = num1 / num2;
			break;
		default:
			resultado = num2;
	}

	textView.value = resultado;
	num1 = 0;
	num2 = resultado;
};

//retorna a caixa de exibição para o valor 0
function limparTela(){
	num1 = 0;
	num2 = 0;
	resultado = 0;
	textView.value = 0;
};